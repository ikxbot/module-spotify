<?php
namespace Ikx\Spotify\Command;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class SpotifyCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    private $clientId = '';
    private $clientSecret = '';
    private $token = '';
    private $tokenType = '';
    private $lastJson = '';

    public function describe()
    {
        return "Weather information command";
    }

    public function run()
    {
        if (count($this->params)) {
            $requestJson = $this->requestApiToken();
            if ($requestJson) {
                $this->token = $requestJson->access_token;
                $this->tokenType = $requestJson->token_type;

                $results = $this->search();

                if (!$results) {
                    $this->msg($this->channel, 'Something went wrong while contacting Spotify API');
                    $this->server->log('Spotify error.');
                    $this->server->log($this->lastJson);
                } else if (!isset($results->tracks)) {
                    $this->msg($this->channel, 'Something went wrong while contacting Spotify API');
                    $this->server->log('Spotify error.');
                    $this->server->log($this->lastJson);
                } else if(!isset($results->tracks->items)) {
                    $this->msg($this->channel, 'No results found');
                    $this->server->log('Spotify error.');
                    $this->server->log('JSON: ' . $this->lastJson);
                } else {
                    foreach($results->tracks->items as $item) {
                        $artists = [];
                        foreach($item->artists as $artist) {
                            $artists[] = $artist->name;
                        }

                        $artists = implode(', ' , $artists);
                        $this->msg($this->channel, sprintf('%s - %s (%d): %s',
                            Format::bold($artists . ' - ' . $item->name),
                            $item->album->name,
                            date('Y', strtotime($item->album->release_date)),
                            Format::underline(Format::color($item->external_urls->spotify, 12))));
                    }
                }
            } else {
                $this->msg($this->channel, 'Something went wrong while contacting Spotify API');
                $this->server->log('Spotify error.');
                $this->server->log($this->lastJson);
            }
        }
    }

    private function search() {
        $query = http_build_query([
            'q'         => implode(' ', $this->params),
            'type'      => 'track',
            'limit'     => '3',
            'offset'    => '0',
            'market'    => 'NL'
        ]);

        echo $query . PHP_EOL;
        echo $this->token . PHP_EOL;

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL                 => 'https://api.spotify.com/v1/search?' . $query,
            CURLOPT_RETURNTRANSFER      => true,
            CURLOPT_HTTPHEADER          => [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->token
            ]
        ]);

        $buffer = curl_exec($ch);
        curl_close($ch);

        $this->lastJson = $buffer;

        return @json_decode($buffer);
    }

    private function requestApiToken() {
        $grantType   = 'client_credentials';
        $redirectUri = 'http://webchat.jrv.cc/spotify.php';
        $q = ['grant_type' => $grantType, 'redirect_uri' => $redirectUri];

        $this->clientId = Application::config()->get('options.spotify.id');
        $this->clientSecret = Application::config()->get('options.spotify.secret');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://accounts.spotify.com/api/token');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($q));
        curl_setopt($ch, CURLOPT_USERPWD, sprintf('%s:%s', $this->clientId, $this->clientSecret));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $buffer = curl_exec($ch);
        curl_close($ch);

        $this->lastJson = $buffer;

        return @json_decode($buffer);
    }
}